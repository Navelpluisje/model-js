import { IControls } from '../sequencer-parts/controls/index';
import glideTemplate from "./template.html"
import glideStyle from "./style.css"

type StepData = {
  note: number,
  length: number,
}
type SequencerData = Array<StepData>

export interface ISequencer extends HTMLElement {
  trigger: Function
}

class Sequencer extends HTMLElement implements ISequencer {
  controlDom: IControls
  playButton: HTMLButtonElement
  stopButton: HTMLButtonElement
  resetButton: HTMLButtonElement
  running: boolean
  steps: number = 8
  currentStep: number = 0
  worker: Worker
  data: Array<StepData> = [{
    note: 400,
    length: .5,
  },{
    note: 440,
    length: .5,
  },{
    note: 480,
    length: .5,
  },{
    note: 520,
    length: .5,
  },{
    note: 300,
    length: .5,
  },{
    note: 320,
    length: .5,
  },{
    note: 340,
    length: .5,
  },{
    note: 300,
    length: .5,
  },]
  trigger: Function


  static instantiated: boolean = false

  constructor() {
    super();
    const template = document.createElement('div');
    template.innerHTML = glideTemplate;
    const templateContent = <HTMLTemplateElement>template.firstChild;
    const shadowRoot = this.attachShadow({ mode: 'open' });

    const styling = document.createElement('style');
    styling.innerHTML = glideStyle;

    shadowRoot.appendChild(templateContent.content.cloneNode(true));
    shadowRoot.appendChild(styling.cloneNode(true));
  }

  connectedCallback() {
    this.worker = new Worker('/dist/workers/sequencer.js')
    this.controlDom = this.shadowRoot.querySelector('seq-controls')
    this.stopButton = this.shadowRoot.querySelector('#stop')
    this.playButton = this.shadowRoot.querySelector('#play')
    this.resetButton = this.shadowRoot.querySelector('#reset')
    this.setEventBindings()
  }

  setEventBindings() {
    this.worker.addEventListener('message', this.handleWorker.bind(this))
    this.controlDom.onStart(this.start.bind(this))
    this.controlDom.onStop(this.stop.bind(this))
    this.controlDom.onReset(this.reset.bind(this))
    this.controlDom.onTempoChange(this.setTempo.bind(this))
  }

  handleWorker({ data }: MessageEvent) {
    if (data !== 'nextStep') { return }
    this.trigger(this.data[this.currentStep])
    this.currentStep += 1
    if (this.currentStep === this.steps) { this.currentStep = 0 }
  }

  start() {
    this.worker.postMessage(['runSequencer', true])
  }

  stop() {
    this.worker.postMessage(['runSequencer', false])
  }

  reset() {
    this.currentStep = 0
  }

  setTempo(tempo: number) {
    this.worker.postMessage(['setSequencerSpeed', Math.round(tempo)])
  }
}

export default () => {
  if (Sequencer.instantiated) { return null }
  Sequencer.instantiated = true
  return customElements.define('np-sequencer', Sequencer)
}
