

export async function createConvolverData(context: AudioContext, convolver: ConvolverNode) {
  let soundSource: AudioBufferSourceNode
  let audioData: ArrayBuffer
  const response = await fetch('/assets/concert-crowd.mp3')
  // const response = await fetch('https://mdn.github.io/voice-change-o-matic/audio/concert-crowd.ogg')
  try {
    audioData = await response.arrayBuffer()
  } catch (e) {
    console.error('could not get data for convolver', e)
  }
  await context.decodeAudioData(audioData, (buffer) => {
    soundSource = context.createBufferSource()
    convolver.buffer = buffer;
  })

  return soundSource
}
