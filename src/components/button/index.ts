import buttonTemplate from './template.html'
import buttonStyle from './style.css'

export interface IButton extends HTMLElement {
}

class Button extends HTMLElement implements IButton {
  button: HTMLButtonElement
  clickEvent: CustomEvent

  constructor() {
    super();
    const bodyTemplate = document.createElement('div')
    bodyTemplate.innerHTML = buttonTemplate
    const templateContent = <HTMLTemplateElement>bodyTemplate.firstChild
    const styling = document.createElement('style')
    styling.innerHTML = buttonStyle

    const shadowRoot = this.attachShadow({ mode: 'open' })
    shadowRoot.appendChild(templateContent.content.cloneNode(true))
    shadowRoot.appendChild(styling.cloneNode(true))

    // Create Events
    this.clickEvent = new CustomEvent('click', {
      bubbles: true,
      cancelable: false,
    });
  }

  connectedCallback() {
    this.button = this.shadowRoot.querySelector('button')
    this.setEventBindings()
  }

  setEventBindings() {
    this.button.addEventListener('click', () => this.dispatchEvent(this.clickEvent))
  }
}

export default () => customElements.define('np-button', Button)

