import html from './template.html';
import style from './style.css';

// const RESISTOR_REGEX = /^([0-9]{2}[kM]|[0-9]{1,9})$/;

class Resistor extends HTMLElement {
  resistor: HTMLElement
  value: string

  constructor() {
    super();
    const template = document.createElement('div');
    template.innerHTML = html;
    const templateContent = template.querySelector('.resistor');

    const styling = document.createElement('style');
    styling.innerHTML = style;

    const shadowRoot = this.attachShadow({ mode: 'open' });
    shadowRoot.appendChild(templateContent.cloneNode(true));
    shadowRoot.appendChild(styling.cloneNode(true));
  }

  connectedCallback() {
    this.resistor = this.shadowRoot.querySelector('.resistor');

    // Get the attributes of our web-component
    this.value = this.getAttribute('value') || '0';

    this.setColorBands();
  }

  setColorBands() {
    let value;
    if (this.value.search('M')) {
      value = parseFloat(this.value) * (10 ** 6);
    } else if (this.value.search('k')) {
      value = parseFloat(this.value) * (10 ** 3);
    } else {
      value = parseFloat(this.value);
    }
    this.setBandColor(0, value.toString()[0]);
    this.setBandColor(1, value.toString()[1] || '0');

    let power;
    for (let i = -2; i < 10; i += 1) {
      if ((10 ** i) > (value / 100)) {
        power = i;
        break;
      }
    }
    this.setBandColor(2, power.toString());
  }

  setBandColor(index: number, value: string) {
    this.shadowRoot.querySelectorAll('.band')[index].className = `band color-${value}`;
  }
};

export default () => customElements.define('np-resistor', Resistor)
