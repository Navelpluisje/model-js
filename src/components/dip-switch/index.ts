import { decimalToBinary } from '../../helpers/decimalToBinary';
import dipSwitchBodyTemplate from './dip-switch-body.template.html';
import dipSwitchItemTemplate from './dip-switch-item.template.html';
import dipSwitchStyle from './dip-switch.css';

export interface IDipSwitch extends HTMLElement {
  value: number
  dips: number
  inputs: NodeListOf<HTMLInputElement>
  showValue: boolean
  dipSwitchContent: ChildNode
  changeEvent: CustomEvent
  setValue(value: number): void
}

class DipSwitch extends HTMLElement implements IDipSwitch {
  value: number
  dips: number
  inputs: NodeListOf<HTMLInputElement>
  showValue: boolean
  dipSwitchContent: ChildNode
  changeEvent: CustomEvent

  constructor() {
    // Always call super first in constructor
    super();
    // Get the template and substract the parts
    // const template = document.getElementById('np-dip-switch').import;
    const bodyTemplate = document.createElement('div')
    bodyTemplate.innerHTML = dipSwitchBodyTemplate
    const templateContent = <HTMLTemplateElement>bodyTemplate.firstChild
    const styling = document.createElement('style')
    styling.innerHTML = dipSwitchStyle

    const shadowRoot = this.attachShadow({ mode: 'open' })
    shadowRoot.appendChild(templateContent.content.cloneNode(true))
    shadowRoot.appendChild(styling.cloneNode(true))

    const dipSwitchTemplate = document.createElement('div')
    dipSwitchTemplate.innerHTML = dipSwitchItemTemplate
    this.dipSwitchContent = dipSwitchTemplate.firstChild

    // Create Events
    this.changeEvent = new CustomEvent('change', {
      bubbles: true,
      cancelable: false,
    });
  }

  connectedCallback() {
    this.value = 0;
    // Get the attributes of our web-component
    this.dips = parseInt(this.getAttribute('dips'), 10) || 1;
    this.showValue = this.getAttribute('show-value') !== null;

    this.appendDips();
    this.setSwitchValue();

    this.inputs = this.shadowRoot.querySelectorAll('input');
  }

  appendDips() {
    const list = this.shadowRoot.querySelector('ul');

    Array(this.dips).fill('-').forEach((x, index) => {
      list.appendChild(this.createDipSwitchItem(<HTMLElement>this.dipSwitchContent.cloneNode(true), index));
    });
  }

  createDipSwitchItem(dipSwitch: HTMLElement, index: number) {
    const input = dipSwitch.querySelector('input');
    const label = dipSwitch.querySelector('label');
    const text = dipSwitch.querySelector('div');
    input.id = `dip-${index.toString()}`;
    input.name = input.id;
    label.setAttribute('for', input.id);
    text.textContent = (index + 1).toString();

    input.addEventListener('change', this.setValueOnChange.bind(this));
    return dipSwitch;
  }

  setValueOnChange() {
    const val: Array<Number> = [];
    this.inputs.forEach((input) => {
      val.push(Number(input.checked));
    });

    this.value = parseInt(val.join(''), 2);
    this.setSwitchValue();
    this.dispatchEvent(this.changeEvent);
  }

  setValue(value: number): void {
    const binArray = decimalToBinary(value, this.inputs.length);
    this.inputs.forEach((input, index) => {
      input.checked = Boolean(binArray[index])
    });
  }

  setSwitchValue() {
    if (this.showValue) {
      const switchValue = this.shadowRoot.querySelector('.dip-switch-value');
      switchValue.textContent = this.value.toString();
    }
  }
}

export default () => customElements.define('dip-switch', DipSwitch)

