import singleSelectBodyTemplate from './single-select-body.template.html'
import singleSelectItemTemplate from './single-select-item.template.html'
import singleSelectStyle from './single-select.css'

export interface ISingleSelect extends HTMLElement {
  values: Array<string>
  value: number
  name: number
  selectContent: ChildNode
  changeEvent: CustomEvent
  setOption(id: number): void
}

class SingleSelect extends HTMLElement implements ISingleSelect {
  values: Array<string>
  value: number
  name: number
  selectContent: ChildNode
  changeEvent: CustomEvent

  constructor() {
    // Always call super first in constructor
    super();
    // Get the template and substract the parts
    // const template = document.getElementById('np-dip-switch').import;
    const bodyTemplate = document.createElement('div')
    bodyTemplate.innerHTML = singleSelectBodyTemplate
    const templateContent = <HTMLTemplateElement>bodyTemplate.firstChild
    const styling = document.createElement('style')
    styling.innerHTML = singleSelectStyle

    const shadowRoot = this.attachShadow({ mode: 'open' })
    shadowRoot.appendChild(templateContent.content.cloneNode(true))
    shadowRoot.appendChild(styling.cloneNode(true))

    const selectTemplate = document.createElement('div')
    selectTemplate.innerHTML = singleSelectItemTemplate
    this.selectContent = selectTemplate.firstChild

    // Create Events
    this.changeEvent = new CustomEvent('change', {
      bubbles: true,
      cancelable: false,
    });
  }

  connectedCallback() {
    this.name = Number(new Date())
    this.values = this.getAttribute('values').split(',');
    this.value = Number(this.getAttribute('value')) || 0
    this.appendItems();
  }

  appendItems() {
    const list = this.shadowRoot.querySelector('ul');

    Array(this.values.length).fill('-').forEach((x, index) => {
      list.appendChild(this.createSelectItem(<HTMLElement>this.selectContent.cloneNode(true), index));
    });
  }

  createSelectItem(select: HTMLElement, index: number) {
    const input = select.querySelector('input')
    const label = select.querySelector('label')
    const text = select.querySelector('label > span')
    input.id = `select-${index.toString()}`
    input.name = this.name.toString()
    input.value = index.toString()
    input.checked = this.value === index
    label.setAttribute('for', input.id)
    text.textContent = this.values[index]

    input.addEventListener('change', ({ target }) => this.setValue(<HTMLInputElement>target))
    return select;
  }

  setValue(target: HTMLInputElement) {
    if (!target.checked) { return false }
    this.value = Number((<HTMLInputElement>target).value)
    this.dispatchEvent(this.changeEvent);
  }

  setOption(id: number): void {
    this.value = id
    const options = this.shadowRoot.querySelectorAll('input')
    options.forEach((input, index) => {
      input.checked = this.value === index
    })
    this.dispatchEvent(this.changeEvent);
  }
}

export default () => customElements.define('single-select', SingleSelect)

