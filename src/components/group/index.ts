import groupTemplate from "./group.template.html"
import groupStyle from "./group.css"

export interface IGroup extends HTMLElement {
  title: string
}

class Group extends HTMLElement implements IGroup {
  title: string

  constructor() {
    super()
    const template = document.createElement('div')
    template.innerHTML = groupTemplate
    const styling = document.createElement('style')
    styling.innerHTML = groupStyle

    const templateContent = <HTMLTemplateElement>template.firstChild
    const shadowRoot = this.attachShadow({ mode: 'open' })

    shadowRoot.appendChild(templateContent.content.cloneNode(true))
    shadowRoot.appendChild(styling.cloneNode(true))
  }

  connectedCallback() {
    this.title = this.getAttribute('title') || 'title here'

    this.shadowRoot.querySelector('legend').innerHTML = this.title
  }
}

export default () => customElements.define('synth-group', Group)
