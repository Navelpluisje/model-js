import ledTemplate from './template.html'
import ledStyle from './style.css'

type ColorType = 'red' | 'yellow' | 'green'
type ColorTypes = {
  [color in ColorType]: number
}

export interface ILed extends HTMLElement {
}

class Led extends HTMLElement implements ILed {
  led: HTMLElement
  value: boolean
  label: string
  color: ColorType
  colors: ColorTypes = {
    'red': 0,
    'yellow': 60,
    'green': 120,
  }

  static get observedAttributes() { return ['color', 'on']; }

  constructor() {
    super();
    const bodyTemplate = document.createElement('div')
    bodyTemplate.innerHTML = ledTemplate
    const templateContent = <HTMLTemplateElement>bodyTemplate.firstChild
    const styling = document.createElement('style')
    styling.innerHTML = ledStyle

    const shadowRoot = this.attachShadow({ mode: 'open' })
    shadowRoot.appendChild(templateContent.content.cloneNode(true))
    shadowRoot.appendChild(styling.cloneNode(true))
  }

  connectedCallback() {
    this.value = this.getAttribute('on') !== null
    this.label = this.getAttribute('title') || ''
    this.color = <ColorType>this.getAttribute('color') || 'red'
    this.led = this.shadowRoot.querySelector('.led')
    this.setLedColor()
    this.setLed()
  }

  attributeChangedCallback(name: string, oldValue: string, newValue: string) {
    if (this.led === undefined) { return }
    switch (name) {
      case 'color':
        this.color = <ColorType>newValue
        this.setLedColor()
      case 'on':
        this.value = this.getAttribute('on') !== null
        this.setLed()
      }
  }

  setLedColor() {
    this.led.style.setProperty('--led-color', this.colors[this.color].toString())
  }

  setLed() {
    if (this.value) {
      this.led.classList.add('on')
    } else {
      this.led.classList.remove('on')
    }
  }

  on() {
    this.value = true
    this.setLed()
  }

  off() {
    this.value = false
    this.setLed()
  }
}

export default () => customElements.define('np-led', Led)

