import { ISingleSelect, IPotMeter } from '@components/index'
import noiseTemplate from "./template.html"
import noiseStyle from "./style.css"

export interface IStep extends HTMLElement {
}

class Step extends HTMLElement implements IStep {
  changeEvent: CustomEvent

  static instantiated: boolean = false

  constructor() {
    super();
    const template = document.createElement('div');
    template.innerHTML = noiseTemplate;
    const templateContent = <HTMLTemplateElement>template.firstChild;
    const shadowRoot = this.attachShadow({ mode: 'open' });

    const styling = document.createElement('style');
    styling.innerHTML = noiseStyle;

    shadowRoot.appendChild(templateContent.content.cloneNode(true));
    shadowRoot.appendChild(styling.cloneNode(true));

    this.changeEvent = new CustomEvent('change', {
      bubbles: true,
      cancelable: false,
    });
  }

  connectedCallback() {
    this.setInitialValues()
    this.setEventBindings()
  }

  setInitialValues() {
  }

  setEventBindings() {
  }
}

export default () => {
  if (Step.instantiated) { return null }
  Step.instantiated = true
  return customElements.define('seq-step', Step)
}
