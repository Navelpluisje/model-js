import { ISingleSelect, IPotMeter } from '@components/index'
import noiseTemplate from "./template.html"
import noiseStyle from "./style.css"

export interface IControls extends HTMLElement {
  onStart(callBack: Function): void
  onStop(callBack: Function): void
  onReset(callBack: Function): void
  onTempoChange(callback: Function): void
}

class Controls extends HTMLElement implements IControls {
  startButton: HTMLElement
  stopButton: HTMLElement
  resetButton: HTMLElement
  controlLed: HTMLElement
  tempoPotMeter: IPotMeter
  start: Function
  stop: Function
  reset: Function
  tempoCallback: Function
  tempo: number
  changeEvent: CustomEvent

  static instantiated: boolean = false

  constructor() {
    super();
    const template = document.createElement('div');
    template.innerHTML = noiseTemplate;
    const templateContent = <HTMLTemplateElement>template.firstChild;
    const shadowRoot = this.attachShadow({ mode: 'open' });

    const styling = document.createElement('style');
    styling.innerHTML = noiseStyle;

    shadowRoot.appendChild(templateContent.content.cloneNode(true));
    shadowRoot.appendChild(styling.cloneNode(true));

    this.changeEvent = new CustomEvent('change', {
      bubbles: true,
      cancelable: false,
    });
  }

  connectedCallback() {
    this.tempo = Number(this.getAttribute('tempo')) || 120
    this.controlLed = this.shadowRoot.querySelector('#sequencer-control-led')
    this.startButton = this.shadowRoot.querySelector('#sequencer-play')
    this.stopButton = this.shadowRoot.querySelector('#sequencer-stop')
    this.resetButton = this.shadowRoot.querySelector('#sequencer-reset')
    this.tempoPotMeter = this.shadowRoot.querySelector('#sequencer-control-tempo')

    this.setInitialValues()
    this.setEventBindings()
  }

  setInitialValues() {
    this.tempoPotMeter.setValue(this.tempo)
  }

  setEventBindings() {
    this.startButton.addEventListener('click', () => {
      this.start()
      this.controlLed.setAttribute('color', 'green')
    })
    this.stopButton.addEventListener('click', () => {
      this.stop()
      this.controlLed.setAttribute('color', 'red')
    })
    this.resetButton.addEventListener('click', () => this.reset())
    this.tempoPotMeter.addEventListener('change', ({ target }) => this.tempoCallback((<HTMLInputElement>target).value))
  }

  onStart(callBack: Function): void {
    this.start = callBack
  }
  onStop(callBack: Function): void {
    this.stop = callBack
    this.controlLed.setAttribute('color', 'red')
  }
  onReset(callBack: Function): void {
    this.reset = callBack
  }
  onTempoChange(callback: Function): void {
    this.tempoCallback = callback
  }

}

export default () => {
  if (Controls.instantiated) { return null }
  Controls.instantiated = true
  return customElements.define('seq-controls', Controls)
}
