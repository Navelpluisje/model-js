import * as synthPart from '@synth-parts/index'
import * as sequencerPart from './sequencer-parts/index'
import {
  DipSwitch,
  Group,
  PotMeter,
  Resistor,
  SingleSelect,
  Led,
  Button,
} from '@components/index'
import Synthesizer, { ISynthesizer } from './synthesizer/index'
import Sequencer, { ISequencer } from './sequencer/index'
import { createConvolverData } from './convolverData'
import { NOTES } from './constants'

DipSwitch()
Group()
PotMeter()
Resistor()
SingleSelect()
Led()
Button()

synthPart.Voice()
synthPart.Mixer()
synthPart.Filter()
synthPart.Envelope()
synthPart.Noise()
synthPart.Distortion()
synthPart.Oscilloscope()
synthPart.Glide()
synthPart.Amplifier()
synthPart.Lfo()

sequencerPart.Controls()
sequencerPart.Step()

Synthesizer()
Sequencer()

const synthesizer: ISynthesizer = document.querySelector('np-synthesizer')
const sequencer: ISequencer = document.querySelector('np-sequencer')

sequencer.trigger = triggerNote

function triggerNote(data: any): void {
  synthesizer.playNote(data.note)
  setTimeout(() => releaseNote(), 300)
}

function playNote({ target }: Event) {
  const note = NOTES[Number((<HTMLInputElement>target).dataset.note)].freq
  synthesizer.playNote(note)
}

function releaseNote() {
  synthesizer.releaseNote()
}


document.querySelectorAll('#keyboard button').forEach(button => {
  button.addEventListener('mousedown', playNote)
  button.addEventListener('mouseup', releaseNote)
})
