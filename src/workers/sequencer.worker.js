let running = false;
let speed = 125;
let current = 0;

const run = () => {
  if (running) {
    setTimeout(run, speed);
  }
  if (current % 4 === 0) {
    postMessage('nextStep');
    current = 0
  } else {
    postMessage('nextQuarter');
  }
  current += 1
};

onmessage = (evt) => {
  switch (evt.data[0]) {
    case 'runSequencer':
    if (!running && evt.data[1]) {
      running = evt.data[1];
      run();
    } else {
      running = evt.data[1];
    }
    break;

  case 'setSequencerSpeed':
    speed = 15000 / evt.data[1];
    break;
  default:
  }
};
