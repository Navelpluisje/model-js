export const round = (number: number, step: number): number => {
  const splitValue = step.toString().split('.')
  const decimals = (splitValue.length > 1 && splitValue[1].length) || 0;
  const value = Math.round(number / step) * step;
  return parseFloat(value.toFixed(decimals));
};
