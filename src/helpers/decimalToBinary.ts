export const decimalToBinary = (value: number, length: number) => {
  const binaries: Array<Number> = [];
  let rest = value;
  for (let i = length; i > 0; i -= 1) {
    const x = Math.pow(2, i - 1)
    if (x <= rest) {
      rest = rest - x;
      binaries.push(1)
    } else {
      binaries.push(0)
    }
  }
  return binaries
}