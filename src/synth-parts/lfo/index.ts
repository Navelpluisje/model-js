import { IDipSwitch, IPotMeter } from '@components/index'
import ampTemplate from "./template.html"
import ampStyle from "./style.css"

type WaveType = 'sine' | 'square' | 'sawtooth' | 'triangle'

export interface ILFO extends HTMLElement {
  getLfoNode(context: AudioContext): OscillatorNode
}

class Lfo extends HTMLElement implements ILFO {
  enabled: boolean
  frequency: number
  wave: WaveType
  context: AudioContext
  lfoNode: OscillatorNode
  enabledNode: IDipSwitch
  frequencyNode: IPotMeter
  waveNode: IDipSwitch
  changeEvent: CustomEvent

  static instantiated: boolean = false
  static waves: Array<WaveType> = [
    'sine',
    'square',
    'sawtooth',
    'triangle',
  ]

  constructor() {
    super();
    const template = document.createElement('div');
    template.innerHTML = ampTemplate;
    const templateContent = <HTMLTemplateElement>template.firstChild;
    const shadowRoot = this.attachShadow({ mode: 'open' });

    const styling = document.createElement('style');
    styling.innerHTML = ampStyle;

    shadowRoot.appendChild(templateContent.content.cloneNode(true));
    shadowRoot.appendChild(styling.cloneNode(true));
  }

  connectedCallback() {
    this.enabled = this.getAttribute('on') !== null
    this.frequency = Number(this.getAttribute('frequency')) || .5
    this.wave = <WaveType>this.getAttribute('wave')
    this.enabledNode = this.shadowRoot.querySelector('#lfo-enabled')
    this.frequencyNode = this.shadowRoot.querySelector('#lfo-frequency')
    this.waveNode = this.shadowRoot.querySelector('#lfo-wave')

    this.setInitialValues()
    this.setEventBindings()
  }

  setInitialValues(): void {
    this.enabledNode.setValue(Number(this.enabled))
    this.frequencyNode.setValue(this.frequency)
    this.waveNode.setValue(Lfo.waves.indexOf(this.wave))
  }

  setEventBindings() {
    this.enabledNode.addEventListener('change', ({ target }): boolean =>
      this.setEnabled(Boolean((<IDipSwitch>target).value))
    )
    this.frequencyNode.addEventListener('change', ({ target }): boolean =>
      this.setFrequency(Number((<IPotMeter>target).value))
    )
    this.waveNode.addEventListener('change', ({ target }): boolean =>
      this.setWave(Number((<IDipSwitch>target).value))
    )
  }

  setEnabled(enabled: boolean): boolean {
    if (this.enabled === enabled) { return false }
    this.enabled = enabled;
    if (this.lfoNode) {
      this.enabled
        ? this.lfoNode.frequency.setValueAtTime(this.frequency, this.context.currentTime)
        : this.lfoNode.frequency.setValueAtTime(0, this.context.currentTime)
    }
    return true
  }

  setFrequency(frequency: number): boolean {
    if (this.frequency === frequency) { return false }
    this.frequency = frequency;
    if (this.lfoNode) {
      this.enabled
        ? this.lfoNode.frequency.setValueAtTime(this.frequency, this.context.currentTime)
        : this.lfoNode.frequency.setValueAtTime(0, this.context.currentTime)
    }
    return true
  }

  setWave(wave: number): boolean {
    if (this.wave === Lfo.waves[wave]) { return false }
    this.wave = Lfo.waves[wave];
    if (this.lfoNode) {
      this.lfoNode.type = this.wave
    }
    return true
  }

  getLfoNode(context: AudioContext): OscillatorNode {
    this.context = context;
    this.lfoNode = this.context.createOscillator()
    this.enabled
      ? this.lfoNode.frequency.setValueAtTime(this.frequency, this.context.currentTime)
      : this.lfoNode.frequency.setValueAtTime(0, this.context.currentTime)
    this.lfoNode.type = this.wave
    this.lfoNode.start(0)

    return this.lfoNode
  }
}

export default () => {
  if (Lfo.instantiated) { return null }
  Lfo.instantiated = true
  return customElements.define('synth-lfo', Lfo)
}
