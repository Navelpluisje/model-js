import { IPotMeter, ISingleSelect } from '@components/index'
import filterTemplate from "./template.html";
import filterStyle from "./style.css";

export interface IFilter extends HTMLElement {
  frequency: number
  setType(filterType: number): boolean
  getFilterNode(context: AudioContext): BiquadFilterNode
  setLfoNode(lfoNode: OscillatorNode): void
}

type FilterTypes = {
  [key: string]: BiquadFilterType
}

class Filter extends HTMLElement implements IFilter {
  frequency: number
  q: number
  modDepth: number
  filterType: BiquadFilterType
  context: AudioContext
  filterNode: BiquadFilterNode
  modDepthNode: GainNode
  lfoNode: OscillatorNode
  modDepthDomNode: IPotMeter
  frequencyDomNode: IPotMeter
  qDomNode: IPotMeter
  typeDomNode: ISingleSelect
  hasLfoNode: boolean = false
  changeEvent: CustomEvent

  static filterTypes: FilterTypes = {
    lp: 'lowpass',
    bp: 'bandpass',
    hp: 'highpass',
  }
  static instantiated: boolean = false

  constructor() {
    super();
    const template = document.createElement('div');
    template.innerHTML = filterTemplate;
    const templateContent = <HTMLTemplateElement>template.firstChild;
    const shadowRoot = this.attachShadow({ mode: 'open' });

    const styling = document.createElement('style');
    styling.innerHTML = filterStyle;

    shadowRoot.appendChild(templateContent.content.cloneNode(true));
    shadowRoot.appendChild(styling.cloneNode(true));

    this.changeEvent = new CustomEvent('change', {
      bubbles: true,
      cancelable: false,
    });
  }

  connectedCallback() {
    this.frequency = parseInt(this.getAttribute('frequency'), 10) || 2000
    this.q = parseInt(this.getAttribute('q'), 10) || 0
    this.filterType = <BiquadFilterType>this.getAttribute('type') || 'lowpass'
    this.modDepth = parseInt(this.getAttribute('mod-depth'), 10) || 0
    this.frequencyDomNode = this.shadowRoot.querySelector('#filter-frequency')
    this.qDomNode = this.shadowRoot.querySelector('#filter-q')
    this.modDepthDomNode = this.shadowRoot.querySelector('#filter-mod-depth')
    this.typeDomNode = this.shadowRoot.querySelector('#filter-type')

    this.setInitialValues()
    this.setEventBindings()
  }

  setInitialValues(): void {
    this.frequencyDomNode.setValue(this.frequency)
    this.qDomNode.setValue(this.q)
    this.modDepthDomNode.setValue(this.modDepth)
    this.typeDomNode.setOption(Object.values(Filter.filterTypes).indexOf(this.filterType))
  }

  setEventBindings() {
    this.frequencyDomNode.addEventListener('change', ({ target }): boolean =>
      this.setFrequency(Number((<IPotMeter>target).value))
    )
    this.qDomNode.addEventListener('change', ({ target }): boolean =>
      this.setQ(Number((<IPotMeter>target).value))
    )
    this.typeDomNode.addEventListener('change', ({ target }): boolean =>
      this.setType(Number((<ISingleSelect>target).value))
    )
    this.modDepthDomNode.addEventListener('change', ({ target }): boolean =>
      this.setModDepth(Number((<IPotMeter>target).value))
    )
  }

  setFrequency(frequency: number): boolean {
    if (this.frequency === frequency) { return false }
    this.frequency = frequency;
    if (this.filterNode) {
      this.filterNode.frequency.setValueAtTime(this.frequency, this.context.currentTime)
    }
    this.dispatchEvent(this.changeEvent)
    return true
  }

  setQ(q: number): boolean {
    if (this.q === q) { return false }
    this.q = q;
    if (this.filterNode) {
      this.filterNode.Q.setValueAtTime(this.q, this.context.currentTime)
    }
    this.dispatchEvent(this.changeEvent)
    return true
  }

  setType(type: number): boolean {
    const filterType = <BiquadFilterType>Object.values(Filter.filterTypes)[type]
    if (this.filterType === filterType) { return false }
    this.filterType = filterType;
    if (this.filterNode) {
      this.filterNode.type = this.filterType
    }
    this.dispatchEvent(this.changeEvent)
    return true
  }

  setModDepth(depth: number): boolean {
    if (this.modDepth === depth) { return false }
    this.modDepth = depth;
    if (this.hasLfoNode) {
      this.modDepthNode.gain.setValueAtTime(this.modDepth, this.context.currentTime)
    }
    this.dispatchEvent(this.changeEvent)
    return true
  }

  setLfoNode(lfoNode: OscillatorNode): void {
    this.hasLfoNode = true
    this.lfoNode = lfoNode
    this.modDepthNode = this.context.createGain()
    this.modDepthNode.gain.setValueAtTime(this.modDepth, this.context.currentTime)
    this.lfoNode.connect(this.modDepthNode)
    this.modDepthNode.connect(this.filterNode.frequency)
  }

  getFilterNode(context: AudioContext): BiquadFilterNode {
    this.context = context;
    this.filterNode = context.createBiquadFilter()
    this.filterNode.type = this.filterType
    this.filterNode.frequency.setValueAtTime(this.frequency, this.context.currentTime)
    this.filterNode.Q.setValueAtTime(this.q, this.context.currentTime)
    return this.filterNode
  }
}

export default () => {
  if (Filter.instantiated) { return null }
  Filter.instantiated = true
  return customElements.define('synth-filter', Filter)
}
