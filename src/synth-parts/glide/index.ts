import { IDipSwitch, IPotMeter } from '@components/index'
import glideTemplate from "./template.html"
import glideStyle from "./style.css"

export interface IGlide extends HTMLElement {
  amount: number
  enabled: boolean
  amountNode: IPotMeter
  enabledNode: IDipSwitch
  changeEvent: CustomEvent
}

class Glide extends HTMLElement implements IGlide {
  amount: number
  enabled: boolean
  amountNode: IPotMeter
  enabledNode: IDipSwitch
  changeEvent: CustomEvent

  static instantiated: boolean = false

  constructor() {
    super();
    const template = document.createElement('div');
    template.innerHTML = glideTemplate;
    const templateContent = <HTMLTemplateElement>template.firstChild;
    const shadowRoot = this.attachShadow({ mode: 'open' });

    const styling = document.createElement('style');
    styling.innerHTML = glideStyle;

    shadowRoot.appendChild(templateContent.content.cloneNode(true));
    shadowRoot.appendChild(styling.cloneNode(true));
  }

  connectedCallback() {
    this.enabled = this.getAttribute('on') !== null
    this.amount = Number(this.getAttribute('amount'))
    this.enabledNode = this.shadowRoot.querySelector('#glide-enabled')
    this.amountNode = this.shadowRoot.querySelector('#glide-amount')

    this.setInitialValues()
    this.setEventBindings()
  }

  setInitialValues(): void {
    this.enabledNode.setValue(Number(this.enabled))
    this.amountNode.setValue(this.amount)
  }

  setEventBindings() {
    this.enabledNode.addEventListener('change', ({ target }): boolean =>
      this.setEnabled(Boolean((<IDipSwitch>target).value))
    )
    this.amountNode.addEventListener('change', ({ target }): boolean =>
      this.setAmount(Number((<IPotMeter>target).value))
    )
  }

  setEnabled(enabled: boolean): boolean {
    if (this.enabled === enabled) { return false }
    this.enabled = enabled;
    return true
  }

  setAmount(amount: number): boolean {
    if (this.amount === amount) { return false }
    this.amount = amount;
    return true
  }
}

export default () => {
  if (Glide.instantiated) { return null }
  Glide.instantiated = true
  return customElements.define('synth-glide', Glide)
}
