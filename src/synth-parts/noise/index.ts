import { ISingleSelect } from '@components/index'
import noiseTemplate from "./template.html"
import noiseStyle from "./style.css"

export type NoiseType = 'pink' | 'white'
export type NoiseBuffersType = {
  [key in NoiseType]: AudioBuffer
};
export type NoiseBufferNodesType = {
  [key in NoiseType]: AudioBufferSourceNode
};

export interface INoise extends HTMLElement {
  noiseType: NoiseType
  getNoiseNodes(context: AudioContext): Promise<NoiseBufferNodesType>
}

class Noise extends HTMLElement implements INoise {
  context: BaseAudioContext
  pinkNoise: AudioBuffer
  whiteNoise: AudioBuffer
  noiseBuffers: NoiseBuffersType
  whiteNoiseNode: AudioBufferSourceNode
  pinkNoiseNode: AudioBufferSourceNode
  noiseType: NoiseType
  typeNode: ISingleSelect
  changeEvent: CustomEvent

  static noiseTypes: Array<NoiseType> = ['white', 'pink']
  static instantiated: boolean = false

  constructor() {
    super();
    const template = document.createElement('div');
    template.innerHTML = noiseTemplate;
    const templateContent = <HTMLTemplateElement>template.firstChild;
    const shadowRoot = this.attachShadow({ mode: 'open' });

    const styling = document.createElement('style');
    styling.innerHTML = noiseStyle;

    shadowRoot.appendChild(templateContent.content.cloneNode(true));
    shadowRoot.appendChild(styling.cloneNode(true));

    this.changeEvent = new CustomEvent('change', {
      bubbles: true,
      cancelable: false,
    });
  }

  async getNoise(type: NoiseType) {
    let soundSource: AudioBuffer
    let noiseData: ArrayBuffer
    const response = await fetch(`/assets/audio/${type}noise.mp3`)
    try {
      noiseData = await response.arrayBuffer()
    } catch (e) {
      console.error('could not get data for convolver', e)
    }
    await this.context.decodeAudioData(noiseData, (buffer) => {
      soundSource = buffer
    })

    return soundSource
  }

  connectedCallback() {
    this.noiseType = <NoiseType>this.getAttribute('type') || 'white'
    this.typeNode = this.shadowRoot.querySelector('#noise-type')

    this.setInitialValues()
    this.setEventBindings()
  }

  setInitialValues(): void {
    this.typeNode.setOption(Noise.noiseTypes.indexOf(this.noiseType))
  }

  setEventBindings() {
    this.typeNode.addEventListener('change', ({ target }): boolean =>
      this.setType(Number((<HTMLInputElement>target).value))
    )
  }

  setType(noiseTypeId: number): boolean {
    const noiseType = Noise.noiseTypes[noiseTypeId]
    if (this.noiseType === noiseType) { return false }
    this.noiseType = noiseType;
    this.dispatchEvent(this.changeEvent)
    return true
  }

  async getNoiseNodes(context: AudioContext): Promise<NoiseBufferNodesType> {
    this.context = <BaseAudioContext>context;
    this.noiseBuffers = {
      pink: await this.getNoise('pink'),
      white: await this.getNoise('white')
    }
    this.whiteNoiseNode = context.createBufferSource()
    this.whiteNoiseNode.buffer = this.noiseBuffers.white
    this.whiteNoiseNode.loop = true
    this.whiteNoiseNode.start(0)
    this.pinkNoiseNode = context.createBufferSource()
    this.pinkNoiseNode.buffer = this.noiseBuffers.pink
    this.pinkNoiseNode.loop = true
    this.pinkNoiseNode.start(0)
    return {
      pink: this.pinkNoiseNode,
      white: this.whiteNoiseNode,
    }
  }
}

export default () => {
  if (Noise.instantiated) { return null }
  Noise.instantiated = true
  return customElements.define('synth-noise', Noise)
}
