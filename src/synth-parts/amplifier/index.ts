import { IDipSwitch, IPotMeter } from '@components/index'
import ampTemplate from "./template.html"
import ampStyle from "./style.css"

export interface IAmplifier extends HTMLElement {
  volume: number
  enabled: boolean
  context: AudioContext
  amplifierNode: GainNode
  volumeNode: IPotMeter
  enabledNode: IDipSwitch
  changeEvent: CustomEvent
  getAmplifierNode(context: AudioContext): GainNode
}

class Amplifier extends HTMLElement implements IAmplifier {
  volume: number
  enabled: boolean
  context: AudioContext
  amplifierNode: GainNode
  volumeNode: IPotMeter
  enabledNode: IDipSwitch
  changeEvent: CustomEvent

  static instantiated: boolean = false

  constructor() {
    super();
    const template = document.createElement('div');
    template.innerHTML = ampTemplate;
    const templateContent = <HTMLTemplateElement>template.firstChild;
    const shadowRoot = this.attachShadow({ mode: 'open' });

    const styling = document.createElement('style');
    styling.innerHTML = ampStyle;

    shadowRoot.appendChild(templateContent.content.cloneNode(true));
    shadowRoot.appendChild(styling.cloneNode(true));

    this.changeEvent = new CustomEvent('change', {
      bubbles: true,
      cancelable: false,
    });
  }

  connectedCallback() {
    this.enabled = this.getAttribute('on') !== null
    this.volume = Number(this.getAttribute('volume')) || .5
    this.enabledNode = this.shadowRoot.querySelector('#amp-enabled')
    this.volumeNode = this.shadowRoot.querySelector('#amp-volume')

    this.setInitialValues()
    this.setEventBindings()
  }

  setInitialValues(): void {
    this.enabledNode.setValue(Number(this.enabled))
    this.volumeNode.setValue(this.volume)
  }

  setEventBindings() {
    this.enabledNode.addEventListener('change', ({ target }: Event): boolean =>
      this.setEnabled(Boolean((<IDipSwitch>target).value))
    )
    this.volumeNode.addEventListener('change', ({ target }: Event): boolean =>
      this.setVolume(Number((<IPotMeter>target).value))
    )
  }

  setEnabled(enabled: boolean): boolean {
    if (this.enabled === enabled) { return false }
    this.enabled = enabled;
    this.dispatchEvent(this.changeEvent)
    return true
  }

  setVolume(volume: number): boolean {
    if (this.volume === volume) { return false }
    this.volume = volume;
    if (this.amplifierNode) {
      this.amplifierNode.gain.setValueAtTime(this.volume, this.context.currentTime)
    }
    return true
  }

  getAmplifierNode(context: AudioContext): GainNode {
    this.context = context;
    this.amplifierNode = this.context.createGain()
    this.amplifierNode.gain.setValueAtTime(this.volume, this.context.currentTime)

    return this.amplifierNode
  }
}

export default () => {
  if (Amplifier.instantiated) { return null }
  Amplifier.instantiated = true
  return customElements.define('synth-amplifier', Amplifier)
}
