import { IPotMeter } from '@components/index'
import envelopeTemplate from "./template.html";
import envelopeStyle from "./style.css";

export interface IEnvelope extends HTMLElement {
  attack: number
  decay: number
  sustain: number
  release: number
  attackNode: IPotMeter
  decayNode: IPotMeter
  sustainNode: IPotMeter
  releaseNode: IPotMeter
  changeEvent: CustomEvent
  setInitialValues(): void
  setEventBindings(): void
  getAdsArray(start: number, base: number, spread: number): Float32Array
  getAdsTime(): number
  getReleaseArray(start:number, end?: number): Float32Array
  getReleaseTime(): number
}

class Envelope extends HTMLElement implements IEnvelope {
  attack: number
  decay: number
  sustain: number
  release: number
  attackNode: IPotMeter
  decayNode: IPotMeter
  sustainNode: IPotMeter
  releaseNode: IPotMeter
  changeEvent: CustomEvent

  static instantiated: boolean = false

  constructor() {
    super();
    const template = document.createElement('div');
    template.innerHTML = envelopeTemplate;
    const templateContent = <HTMLTemplateElement>template.firstChild;
    const shadowRoot = this.attachShadow({ mode: 'open' });

    const styling = document.createElement('style');
    styling.innerHTML = envelopeStyle;

    shadowRoot.appendChild(templateContent.content.cloneNode(true));
    shadowRoot.appendChild(styling.cloneNode(true));

    this.changeEvent = new CustomEvent('change', {
      bubbles: true,
      cancelable: false,
    });
  }

  connectedCallback() {
    this.attack = Number(this.getAttribute('attack')) || 0.0001
    this.decay = Number(this.getAttribute('decay')) || 0.0001
    this.sustain = Number(this.getAttribute('sustain')) || 0.0001
    this.release = Number(this.getAttribute('release')) || 0.0001
    this.attackNode = this.shadowRoot.querySelector('#envelope-attack')
    this.decayNode = this.shadowRoot.querySelector('#envelope-decay')
    this.sustainNode = this.shadowRoot.querySelector('#envelope-sustain')
    this.releaseNode = this.shadowRoot.querySelector('#envelope-release')

    this.setInitialValues()
    this.setEventBindings()
  }

  setInitialValues(): void {
    this.attackNode.setValue(this.attack)
    this.decayNode.setValue(this.decay)
    this.sustainNode.setValue(this.sustain)
    this.releaseNode.setValue(this.release)
  }

  setEventBindings(): void {
    this.attackNode.addEventListener('change', ({ target }): boolean =>
      this.setAttack(Number((<HTMLInputElement>target).value))
    )
    this.decayNode.addEventListener('change', ({ target }): boolean =>
      this.setDecay(Number((<HTMLInputElement>target).value))
    )
    this.sustainNode.addEventListener('change', ({ target }): boolean =>
      this.setSustain(Number((<HTMLInputElement>target).value))
    )
    this.releaseNode.addEventListener('change', ({ target }): boolean =>
      this.setRelease(Number((<HTMLInputElement>target).value))
    )
  }

  setAttack(attack: number): boolean {
    if (this.attack === attack) { return false }
    this.attack = attack;
    this.dispatchEvent(this.changeEvent)
    return true
  }

  setDecay(decay: number): boolean {
    if (this.decay === decay) { return false }
    this.decay = decay;
    this.dispatchEvent(this.changeEvent)
    return true
  }

  setSustain(sustain: number): boolean {
    if (this.sustain === sustain) { return false }
    this.sustain = sustain;
    this.dispatchEvent(this.changeEvent)
    return true
  }

  setRelease(release: number): boolean {
    if (this.release === release) { return false }
    this.release = release;
    this.dispatchEvent(this.changeEvent)
    return true
  }

  /**
   * Get the ads values for the envelope
   * @param start The start value for the envelope
   * @param base The base where the envelop should be added to
   * @param spread The amount of the base used to add
   */
  getAdsArray(start: number, base: number = 3000, spread: number = 0.1) {
    const from = start || base || 0
    const to = base > 0 ? base + this.decay + base * spread : this.decay
    const adsArray = new Float32Array(5)
    adsArray[0] = from
    adsArray[1] = Math.abs(to - from) * 0.333
    adsArray[2] = Math.abs(to - from) * 0.666
    adsArray[3] = to
    adsArray[4] = this.sustain * to
    return adsArray
  }

  getAdsTime() {
    return this.attack * 4 || 0.00001;
  }

  getReleaseArray(start: number | null = null, end: number = 0): Float32Array {
    const releaseArray = new Float32Array(2)
    releaseArray[0] = start === null ? this.sustain : start
    releaseArray[1] = end
    return releaseArray;
  }

  getReleaseTime() {
    return this.release * 3 || 0.0001;
  }
}

export default () => {
  if (Envelope.instantiated) { return null }
  Envelope.instantiated = true
  return customElements.define('synth-envelope', Envelope)
}
