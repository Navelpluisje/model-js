import { IPotMeter, IDipSwitch } from '@components/index'
import mixerTemplate from "./mixer.template.html";
import voiceStyle from "./mixer.css";

export interface IMixer extends HTMLElement {
  gain: number
  toggle: boolean
  context: AudioContext
  gainNode: GainNode
  gainDomNode: IPotMeter
  toggleNode: IDipSwitch
  changeEvent: CustomEvent
  getGain(): number
  getMixerNode(context: AudioContext): GainNode
}

class Mixer extends HTMLElement implements IMixer {
  gain: number
  toggle: boolean
  context: AudioContext
  gainNode: GainNode
  gainDomNode: IPotMeter
  toggleNode: IDipSwitch
  changeEvent: CustomEvent

  static instantiated: boolean = false

  constructor() {
    super();
    const template = document.createElement('div');
    template.innerHTML = mixerTemplate;
    const templateContent = <HTMLTemplateElement>template.firstChild;
    const shadowRoot = this.attachShadow({ mode: 'open' });

    const styling = document.createElement('style');
    styling.innerHTML = voiceStyle;

    shadowRoot.appendChild(templateContent.content.cloneNode(true));
    shadowRoot.appendChild(styling.cloneNode(true));

    this.changeEvent = new CustomEvent('change', {
      bubbles: true,
      cancelable: false,
    });
  }

  connectedCallback() {
    this.gain = Number(this.getAttribute('gain')) || 0.5
    this.toggle = this.getAttribute('on') !== null ? true : false
    this.gainDomNode = this.shadowRoot.querySelector('#mixer-gain')
    this.toggleNode = this.shadowRoot.querySelector('#mixer-toggle')

    this.setInitialValues()
    this.setEventBindings()
  }

  setInitialValues(): void {
    this.gainDomNode.setValue(this.gain)
    this.toggleNode.setValue(Number(this.toggle))
  }

  setEventBindings() {
    this.gainDomNode.addEventListener('change', ({ target }): boolean =>
      this.setGain(Number((<IPotMeter>target).value))
    )
    this.toggleNode.addEventListener('change', ({ target }): boolean =>
      this.setToggle(Boolean((<IDipSwitch>target).value))
    )
  }

  setGain(gain: number): boolean {
    if (this.gain === gain) { return false }
    this.gain = gain;
    this.dispatchEvent(this.changeEvent)
    this.setNodeGain()
    return true
  }

  setToggle(toggle: boolean): boolean {
    if (this.toggle === toggle) { return false }
    this.toggle = toggle;
    this.dispatchEvent(this.changeEvent)
    this.setNodeGain()
    return true
  }

  setNodeGain() {
    this.gainNode.gain.setValueAtTime(this.getGain(), this.context.currentTime)
  }

  getGain(): number {
    if (!this.toggle) { return 0 }
    return this.gain
  }

  getMixerNode(context: AudioContext): GainNode {
    this.context = context;
    this.gainNode = context.createGain()
    this.setNodeGain()
    return this.gainNode
  }
}

export default () => {
  if (Mixer.instantiated) { return null }
  Mixer.instantiated = true
  return customElements.define('synth-mixer', Mixer)
}
