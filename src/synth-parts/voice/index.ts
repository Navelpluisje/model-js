import { IDipSwitch, IPotMeter } from '@components/index';
import voiceTemplate from "./template.html"
import voiceStyle from "./style.css"

type WaveType = 'sine' | 'square' | 'sawtooth' | 'triangle'

export interface IVoice extends HTMLElement {
  getOscillatorNode(context: AudioContext): OscillatorNode
  setLfoNode(lfoNode: OscillatorNode): void
}

class Voice extends HTMLElement implements IVoice {
  static waves: Array<OscillatorType> = [
    'sine',
    'square',
    'sawtooth',
    'triangle'
  ]
  note: number
  detune: number
  modDepth: number
  wave: OscillatorType
  context: AudioContext
  oscillatorNode: OscillatorNode
  modDepthNode: GainNode
  lfoNode: OscillatorNode
  detuneInput: IPotMeter
  modDepthDomNode: IPotMeter
  waveSelect: IDipSwitch
  hasLfoNode: boolean = false
  octaveSelect: IPotMeter
  changeEvent: CustomEvent

  constructor() {
    super()
    const template = document.createElement('div')
    template.innerHTML = voiceTemplate
    const styling = document.createElement('style')
    styling.innerHTML = voiceStyle

    const templateContent = <HTMLTemplateElement>template.firstChild
    const shadowRoot = this.attachShadow({ mode: 'open' })

    shadowRoot.appendChild(templateContent.content.cloneNode(true))
    shadowRoot.appendChild(styling.cloneNode(true))

    this.changeEvent = new CustomEvent('change', {
      bubbles: true,
      cancelable: false,
    })
  }

  connectedCallback() {
    this.note = 440
    this.detune = parseInt(this.getAttribute('detune'), 10) || 0
    this.modDepth = parseInt(this.getAttribute('mod-depth'), 10) || 0
    this.wave = <WaveType> this.getAttribute('wave') || 'sine'
    this.detuneInput = this.shadowRoot.querySelector('#voice-detune')
    this.waveSelect = this.shadowRoot.querySelector('dip-switch')
    this.modDepthDomNode = this.shadowRoot.querySelector('#voice-mod-depth')

    this.detuneInput.setValue(this.detune)
    this.waveSelect.setValue(Voice.waves.indexOf(this.wave))
    this.setEventBindings()
  }

  setEventBindings() {
    this.detuneInput.addEventListener(
      'change',
      ({ target }): boolean => this.setDetune(Number((<HTMLInputElement>target).value)),
    )
    this.waveSelect.addEventListener(
      'change',
      ({ target }):boolean => this.setType(Number((<HTMLInputElement>target).value))
    )
    this.modDepthDomNode.addEventListener('change', ({ target }): boolean =>
      this.setModDepth(Number((<IPotMeter>target).value))
    )
  }

  setDetune(detune: number) {
    if (this.detune === detune) { return false }
    this.detune = detune
    if (this.oscillatorNode) {
      this.oscillatorNode.detune.setValueAtTime(detune, this.context.currentTime)
    }
    this.dispatchEvent(this.changeEvent)
    return true
  }

  setType(waveId: number) {
    if (this.wave === Voice.waves[waveId]) { return false }
    this.wave = Voice.waves[waveId]
    if (this.oscillatorNode) {
      this.oscillatorNode.type = this.wave
    }
    this.dispatchEvent(this.changeEvent)
    return true
  }

  setModDepth(depth: number): boolean {
    if (this.modDepth === depth) { return false }
    this.modDepth = depth;
    if (this.hasLfoNode) {
      this.modDepthNode.gain.setValueAtTime(this.modDepth, this.context.currentTime)
    }
    this.dispatchEvent(this.changeEvent)
    return true
  }

  setLfoNode(lfoNode: OscillatorNode): void {
    this.hasLfoNode = true
    this.lfoNode = lfoNode
    this.modDepthNode = this.context.createGain()
    this.modDepthNode.gain.setValueAtTime(this.modDepth, this.context.currentTime)
    this.lfoNode.connect(this.modDepthNode)
    this.modDepthNode.connect(this.oscillatorNode.frequency)
  }

  getOscillatorNode(context: AudioContext): OscillatorNode {
    this.context = context;
    this.oscillatorNode = context.createOscillator()
    this.oscillatorNode.detune.setValueAtTime(this.detune, this.context.currentTime)
    this.oscillatorNode.frequency.setValueAtTime(this.note, this.context.currentTime)
    this.oscillatorNode.type = this.wave
    this.oscillatorNode.start(0)
    return this.oscillatorNode
  }
}

export default () => customElements.define('synth-voice', Voice)
