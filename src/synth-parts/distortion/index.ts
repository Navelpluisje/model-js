import { IDipSwitch, IPotMeter, ISingleSelect } from '@components/index';
import distortionTemplate from "./template.html";
import distortionStyle from "./style.css";

export interface IDistortion extends HTMLElement {
  amount: number
  enabled: boolean
  oversample: OverSampleType
  context: AudioContext
  distortionNode: WaveShaperNode
  amountNode: IPotMeter
  enabledNode: IDipSwitch
  oversampleNode: ISingleSelect
  changeEvent: CustomEvent
  getValue(): number
  getDistortionNode(context: AudioContext): WaveShaperNode
}

class Distortion extends HTMLElement implements IDistortion {
  amount: number
  enabled: boolean
  oversample: OverSampleType
  context: AudioContext
  distortionNode: WaveShaperNode
  amountNode: IPotMeter
  enabledNode: IDipSwitch
  oversampleNode: ISingleSelect
  changeEvent: CustomEvent

  static oversampleTypes: Array<OverSampleType> = ['4x', '2x', 'none']
  static instantiated: boolean = false

  constructor() {
    super();
    const template = document.createElement('div');
    template.innerHTML = distortionTemplate;
    const templateContent = <HTMLTemplateElement>template.firstChild;
    const shadowRoot = this.attachShadow({ mode: 'open' });

    const styling = document.createElement('style');
    styling.innerHTML = distortionStyle;

    shadowRoot.appendChild(templateContent.content.cloneNode(true));
    shadowRoot.appendChild(styling.cloneNode(true));

    this.changeEvent = new CustomEvent('change', {
      bubbles: true,
      cancelable: false,
    });
  }

  connectedCallback() {
    this.amount = parseInt(this.getAttribute('amount'), 10) || 400
    this.enabled = this.getAttribute('amount') !== null
    this.oversample = <OverSampleType>this.getAttribute('oversample') || '4x'
    this.amountNode = this.shadowRoot.querySelector('#distortion-amount')
    this.enabledNode = this.shadowRoot.querySelector('#distortion-enable')
    this.oversampleNode = this.shadowRoot.querySelector('#distortion-oversample')

    this.setInitialValues()
    this.setEventBindings()
  }

  setInitialValues(): void {
    this.amountNode.setValue(this.amount)
    this.enabledNode.setValue(Number(this.enabled))
    this.oversampleNode.setOption(Distortion.oversampleTypes.indexOf(this.oversample))
  }

  setEventBindings() {
    this.amountNode.addEventListener('change', ({ target }): boolean =>
      this.setAmount(Number((<IPotMeter>target).value))
    )
    this.enabledNode.addEventListener('change', ({ target }): boolean =>
      this.setEnabled(Boolean((<IDipSwitch>target).value))
    )
    this.oversampleNode.addEventListener('change', ({ target }): boolean =>
      this.setOversample(Distortion.oversampleTypes[(<ISingleSelect>target).value])
    )
  }

  setAmount(amount: number): boolean {
    if (this.amount === amount) { return false }
    this.amount = amount;
    if (this.distortionNode) {
      this.distortionNode.curve = this.makeDistortionCurve()
    }
    // this.dispatchEvent(this.changeEvent)
    return true
  }

  setEnabled(enabled: boolean): boolean {
    if (this.enabled === enabled) { return false }
    this.enabled = enabled;
    if (this.distortionNode) {
      this.distortionNode.curve = this.makeDistortionCurve()
    }
    this.dispatchEvent(this.changeEvent)
    return true
  }

  setOversample(oversample: OverSampleType): boolean {
    if (this.oversample === oversample) { return false }
    this.oversample = oversample;
    if (this.distortionNode) {
      this.distortionNode.oversample = this.oversample
    }
    // this.dispatchEvent(this.changeEvent)
    return true
  }

  getValue(): number {
    if (this.enabled) { return this.amount }
    return 0
  }

  // distortion curve for the waveshaper, thanks to Kevin Ennis
  // http://stackoverflow.com/questions/22312841/waveshaper-node-in-webaudio-how-to-emulate-distortion
  makeDistortionCurve(): Float32Array {
    const amount = this.getValue()
    const nbSamples = 44100
    const curve = new Float32Array(nbSamples)
    const deg = Math.PI / 180

    for (let i = 0; i < nbSamples; ++i) {
      const x = i * 2 / nbSamples - 1;
      curve[i] = ( 3 + amount ) * x * 20 * deg / ( Math.PI + amount * Math.abs(x) );
    }
    return curve;
  }

  getDistortionNode(context: AudioContext): WaveShaperNode {
    this.context = context
    this.distortionNode = this.context.createWaveShaper()
    this.distortionNode.oversample = 'none'
    this.distortionNode.curve = this.makeDistortionCurve()
    return this.distortionNode
  }
}

export default () => {
  if (Distortion.instantiated) { return null }
  Distortion.instantiated = true
  return customElements.define('synth-distortion', Distortion)
}
