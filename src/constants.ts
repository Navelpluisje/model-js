export type NoteType = {
  freq: number,
  name: string,
  id: string,
}

export const NOTES: Array<NoteType> = [
  {freq: 65.41, name: "C", id: "c"},
  {freq: 69.30, name: "C#", id: "cc"},
  {freq: 73.42, name: "D", id: "d"},
  {freq: 77.78, name: "D#", id: "dd"},
  {freq: 82.41, name: "E", id: "e"},
  {freq: 87.31, name: "F", id: "f"},
  {freq: 92.50, name: "F#", id: "ff"},
  {freq: 98.00, name: "G", id: "g"},
  {freq: 103.83, name: "G#", id: "gg"},
  {freq: 110.00, name: "A", id: "a"},
  {freq: 116.54, name: "A#", id: "aa"},
  {freq: 123.47, name: "B", id: "b"}
]
