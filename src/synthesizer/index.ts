import * as synthPart from '@synth-parts/index'
import glideTemplate from "./template.html"
import glideStyle from "./style.css"

type TriggerData = {
  note: number,
  length: number,
}

export interface ISynthesizer extends HTMLElement {
  playNote(note: number): void
  releaseNote(): void
}

class Synthesizer extends HTMLElement implements ISynthesizer {
  oscillator_1: synthPart.IVoice
  oscillator_2: synthPart.IVoice
  oscillator_3: synthPart.IVoice
  lfo: synthPart.ILFO
  noise: synthPart.INoise
  mixer_1: synthPart.IMixer
  mixer_2: synthPart.IMixer
  mixer_3: synthPart.IMixer
  mixer_noise: synthPart.IMixer
  filter: synthPart.IFilter
  filterEnvelope: synthPart.IEnvelope
  distortion: synthPart.IDistortion
  amplifier: synthPart.IAmplifier
  envelope: synthPart.IEnvelope
  glide: synthPart.IGlide
  oscilloscope: synthPart.IOscilloscope
  // Modes
  context: AudioContext
  gainNode: GainNode
  oscillatorNode_1: OscillatorNode
  oscillatorNode_2: OscillatorNode
  oscillatorNode_3: OscillatorNode
  filterNode: BiquadFilterNode
  distortionNode: WaveShaperNode
  noiseNode: synthPart.NoiseBufferNodesType
  mixerNoiseNode: GainNode
  // States
  distortionEnabled: boolean
  noiseType: synthPart.NoiseType

  static instantiated: boolean = false

  constructor() {
    super();
    const template = document.createElement('div');
    template.innerHTML = glideTemplate;
    const templateContent = <HTMLTemplateElement>template.firstChild;
    const shadowRoot = this.attachShadow({ mode: 'open' });

    const styling = document.createElement('style');
    styling.innerHTML = glideStyle;

    shadowRoot.appendChild(templateContent.content.cloneNode(true));
    shadowRoot.appendChild(styling.cloneNode(true));
  }

  connectedCallback() {
    this.oscillator_1 = this.shadowRoot.querySelector('#osc-1')
    this.oscillator_2 = this.shadowRoot.querySelector('#osc-2')
    this.oscillator_3 = this.shadowRoot.querySelector('#osc-3')
    this.lfo = this.shadowRoot.querySelector('synth-lfo')
    this.noise = this.shadowRoot.querySelector('synth-noise')
    this.mixer_1 = this.shadowRoot.querySelector('#mixer-osc-1')
    this.mixer_2 = this.shadowRoot.querySelector('#mixer-osc-2')
    this.mixer_3 = this.shadowRoot.querySelector('#mixer-osc-3')
    this.mixer_noise = this.shadowRoot.querySelector('#mixer-osc-noise')
    this.filter = this.shadowRoot.querySelector('synth-filter')
    this.filterEnvelope = this.shadowRoot.querySelector('#filter-envelope')
    this.distortion = this.shadowRoot.querySelector('synth-distortion')
    this.amplifier = this.shadowRoot.querySelector('synth-amplifier')
    this.envelope = this.shadowRoot.querySelector('#jsa-envelope')
    this.glide = this.shadowRoot.querySelector('synth-glide')
    this.oscilloscope = this.shadowRoot.querySelector('synth-oscilloscope')

    this.setEventBindings()
  }

  setEventBindings() {
    this.amplifier.addEventListener('change', () => {
      if (this.amplifier.enabled) {
        this.start()
      }
    })
    this.distortion.addEventListener('change', this.toggleDistortion.bind(this))
    this.noise.addEventListener('change', this.toggleNoiseType.bind(this))
  }

  async start() {
    this.context = new AudioContext()

    this.distortionEnabled = this.distortion.enabled
    this.noiseType = this.noise.noiseType

    this.gainNode = this.context.createGain()
    this.gainNode.gain.setValueAtTime(0, this.context.currentTime)

    const merger = this.context.createChannelMerger(2)

    const lfoNode = this.lfo.getLfoNode(this.context)
    this.oscillatorNode_1 = this.oscillator_1.getOscillatorNode(this.context)
    this.oscillator_1.setLfoNode(lfoNode)
    this.oscillatorNode_2 = this.oscillator_2.getOscillatorNode(this.context)
    this.oscillator_2.setLfoNode(lfoNode)
    this.oscillatorNode_3 = this.oscillator_3.getOscillatorNode(this.context)
    this.oscillator_3.setLfoNode(lfoNode)
    this.noiseNode = await this.noise.getNoiseNodes(this.context)

    const mixerNode_1 = this.mixer_1.getMixerNode(this.context)
    const mixerNode_2 = this.mixer_2.getMixerNode(this.context)
    const mixerNode_3 = this.mixer_3.getMixerNode(this.context)
    this.mixerNoiseNode = this.mixer_noise.getMixerNode(this.context)

    this.filterNode = this.filter.getFilterNode(this.context)
    this.filter.setLfoNode(lfoNode)
    this.distortionNode = this.distortion.getDistortionNode(this.context)
    const oscilloscopeNode = this.oscilloscope.getOscilloscopeNode(this.context)
    const amplifierNode = this.amplifier.getAmplifierNode(this.context)

    this.oscillatorNode_1.connect(mixerNode_1)
    this.oscillatorNode_2.connect(mixerNode_2)
    this.oscillatorNode_3.connect(mixerNode_3)
    this.noiseNode[this.noise.noiseType].connect(this.mixerNoiseNode)

    mixerNode_1.connect(merger, 0, 0)
    mixerNode_2.connect(merger, 0, 0)
    mixerNode_3.connect(merger, 0, 0)
    mixerNode_1.connect(merger, 0, 1)
    mixerNode_2.connect(merger, 0, 1)
    mixerNode_3.connect(merger, 0, 1)

    this.mixerNoiseNode.connect(merger, 0, 0)
    this.mixerNoiseNode.connect(merger, 0, 1)

    merger.connect(this.filterNode)
    if (this.distortionEnabled) {
      this.filterNode.connect(this.distortionNode)
      this.distortionNode.connect(this.gainNode)
    } else {
      this.filterNode.connect(this.gainNode)
    }

    // const delay = this.context.createDelay()
    // delay.delayTime.setValueAtTime(.5, this.context.currentTime)
    // const feedback = this.context.createGain()
    // feedback.gain.setValueAtTime(.6, this.context.currentTime)
    // const delayFilter = this.context.createBiquadFilter()
    // delayFilter.frequency.setValueAtTime(1000, this.context.currentTime)
    // delayFilter.Q.setValueAtTime(0, this.context.currentTime)

    // delay.connect(delayFilter)
    // delayFilter.connect(feedback)
    // feedback.connect(delay)
    // this.gainNode.connect(delay)
    // delay.connect(amplifierNode)

    this.gainNode.connect(amplifierNode)
    amplifierNode.connect(oscilloscopeNode)
    oscilloscopeNode.connect(this.context.destination)

  }

  playNote(note: number): void {
    this.gainNode.gain.cancelAndHoldAtTime(0)
    this.filterNode.frequency.cancelAndHoldAtTime(0)

    const glideDuration = this.context.currentTime + (this.glide.enabled ? this.glide.amount : 0)

    this.oscillatorNode_1.frequency.linearRampToValueAtTime(note, glideDuration)
    this.oscillatorNode_2.frequency.linearRampToValueAtTime(note, glideDuration)
    this.oscillatorNode_3.frequency.linearRampToValueAtTime(note, glideDuration)

    this.gainNode.gain.setValueCurveAtTime(
      this.envelope.getAdsArray(this.gainNode.gain.value, 0, 0),
      this.context.currentTime,
      this.envelope.getAdsTime()
    )
    this.filterNode.frequency.setValueCurveAtTime(
      this.filterEnvelope.getAdsArray(this.filterNode.frequency.value || 0.001, this.filter.frequency, 0.1),
      this.context.currentTime,
      this.filterEnvelope.getAdsTime()
    )
  }

  releaseNote(): void {
    this.gainNode.gain.cancelAndHoldAtTime(0)
    this.gainNode.gain.setValueCurveAtTime(
      this.envelope.getReleaseArray(this.gainNode.gain.value),
      this.context.currentTime,
      this.envelope.getReleaseTime()
    )
    this.filterNode.frequency.cancelAndHoldAtTime(0)
    this.filterNode.frequency.setValueCurveAtTime(
      this.filterEnvelope.getReleaseArray(this.filterNode.frequency.value, this.filter.frequency),
      this.context.currentTime,
      this.filterEnvelope.getReleaseTime()
    )
  }

  toggleDistortion(): boolean {
    alert('distortion')
    if (this.distortion.enabled === this.distortionEnabled) { return false}
    this.distortionEnabled = this.distortion.enabled
    if (this.distortionEnabled) {
      this.filterNode.disconnect()
      this.filterNode.connect(this.distortionNode)
      this.distortionNode.connect(this.gainNode)
    } else {
      this.filterNode.disconnect()
      this.distortionNode.disconnect()
      this.filterNode.connect(this.gainNode)
    }
    return true
  }

  toggleNoiseType() : boolean {
    if (this.noiseType === this.noise.noiseType) { return false}
    this.noiseNode[this.noiseType].disconnect()
    this.noiseNode[this.noise.noiseType].connect(this.mixerNoiseNode)
    this.noiseType = this.noise.noiseType
  }

}

export default () => {
  if (Synthesizer.instantiated) { return null }
  Synthesizer.instantiated = true
  return customElements.define('np-synthesizer', Synthesizer)
}
