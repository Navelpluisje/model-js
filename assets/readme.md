# Audiofiles

## Noise
The [pink noise](https://www.audiocheck.net/testtones_pinknoise.php) and [white noise](https://www.audiocheck.net/testtones_whitenoise.php) files are taken from [Audiocheck.net](http://audiocheck.net)

## Convolution sounds
https://fokkie.home.xs4all.nl/IR.htm